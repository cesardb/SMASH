#!/bin/bash
echo "###########################################"
echo "###          STARTING SMASH             ###"
echo "###          DEM PRODUCTION             ###"
echo -e "###########################################\n \n"
date

echo "##################################################################"
echo "############            Read Parameters              #############"
echo -e "##################################################################\n \n"

source SMASH_a_DEM_init.txt # set the variables for the run (folders, dates, parameters files)
source mySMASHtoolbox.sh # import functions

if [ ! -f $st_file0 ];
then
echo "!! Could not find stereo file !!"
echo $st_file0
fi

if [ ! -f $st_file ];
then
echo "!! Could not find stereo file !!"
echo $st_file
fi

# folder where the coarse resolution product are written
outputFolderCoarse=$outputFolderDEM"/PL_"$DATE"_GSD"$GSDcoarse
# folder where the fine resolution product are written
outputFolderFine=$outputFolderDEM"/PL_"$DATE"_GSD"$GSDfine

mkdir -p $outputFolderCoarse
mkdir -p $outputFolderFine

echo "###########################################"
echo "###        Finding images               ###"
echo -e "###########################################\n \n"

# removes extra-information from DATE such as N(orth), S(outh)
date_im=$(echo $DATE | sed -r 's/[^0-9]//g')

# finds images ! correspondance between number 1, 2, 3 and front, nadir, back varies !
inputIMG1=$(find $dataFolderIMG -name IMG*_P_*-001*.JP2) # Front
inputIMG2=$(find $dataFolderIMG -name IMG*_P_*-002*.JP2) # Back
inputIMG3=$(find $dataFolderIMG -name IMG*_P_*-003*.JP2) # Nadir

# finds RPC files
inputRPC1=$(find $dataFolderIMG -name RPC*_P_*-001.XML)
inputRPC2=$(find $dataFolderIMG -name RPC*_P_*-002.XML)
inputRPC3=$(find $dataFolderIMG -name RPC*_P_*-003.XML)

echo "########################################################################"
echo "###                Calculating coarse DEM                            ###"
echo -e "########################################################################\n \n"

# output prefix for the coarse products
outputPrefixCoarse=$(basename $inputIMG1 | cut -d_ -f4)"_GSD"$GSDcoarse

cd $outputFolderCoarse
# calculates point cloud from raw images
$ASP/parallel_stereo --threads-multiprocess 4 --threads-singleprocess 20 --processes 4 -t rpc -s $st_file0 --alignment-method homography  $inputIMG1 $inputIMG3 $inputIMG2 $inputRPC1 $inputRPC3 $inputRPC2 ./$outputPrefixCoarse 
# calculate DEM from point cloud
$ASP/point2dem --nodata-value -9999 --tr $GSDcoarse --errorimage --dem-hole-fill-len 50 --datum WGS_1984 --t_srs $targetSRS $outputPrefixCoarse-PC.tif
# calculate hillshade for visualisation
$ASP/hillshade $outputPrefixCoarse-DEM.tif

inDEM=$outputPrefixCoarse"-DEM.tif"
# projects (ortho-rectifies) images onto coarse DEM
$ASP/mapproject -t rpc $inDEM $inputIMG1 $inputRPC1 ./$outputPrefixCoarse'_P_001_mproj.tif'
$ASP/mapproject -t rpc $inDEM $inputIMG2 $inputRPC2 ./$outputPrefixCoarse'_P_002_mproj.tif'
$ASP/mapproject -t rpc $inDEM $inputIMG3 $inputRPC3 ./$outputPrefixCoarse'_P_003_mproj.tif'

echo "########################################################################"
echo "###                Calculating fine DEM                             ###"
echo -e "########################################################################\n \n"

cd $outputFolderFine

# output prefix for the fine products
outputPrefixFine=$(basename $inputIMG1 | cut -d_ -f4)"_GSD"$GSDfine
inDEMcoarse=$outputFolderCoarse/*"-DEM.tif"

# calculates point cloud from projected images
$ASP/parallel_stereo --processes 3 --threads-multiprocess 6 --threads-singleprocess 24 -t rpcmaprpc -s $st_file $outputFolderCoarse/$outputPrefixCoarse'_P_001_mproj.tif' $outputFolderCoarse/$outputPrefixCoarse'_P_003_mproj.tif' $outputFolderCoarse/$outputPrefixCoarse'_P_002_mproj.tif' $inputRPC1 $inputRPC3 $inputRPC2 ./$outputPrefixFine $inDEMcoarse
# calculates DEM from point cloud
$ASP/point2dem --nodata-value -9999 --tr $GSDfine --errorimage --datum WGS_1984 --t_srs $targetSRS $outputPrefixFine-PC.tif  # calculate DEM from point cloud
# calculates hillshade for visualisation
$ASP/hillshade $outputPrefixFine-DEM.tif # calculate a hillshade for visualisation

## creates a filled DEM used for images projection
gdal_merge.py  $outputFolderCoarse/*"-DEM.tif" $outputPrefixFine'-DEM.tif' -ps $GSDfine $GSDfine -init -9999 -a_nodata -9999 -o ./$outputPrefixFine'-DEM_filled.tif'

# projects nadir image onto fine filled DEM 
DEM4proj=$outputFolderFine/$outputPrefixFine'-DEM_filled.tif'
$ASP/mapproject -t rpc  ./$outputPrefixFine'-DEM.tif' $inputIMG3 $inputRPC3 ./$outputPrefixFine'_P_mproj.tif'

# masking DEM for saturated pixels in PAN
mySuperImposeOTB --IMGref $outputPrefixFine'-DEM.tif' --IMGmob ./$outputPrefixFine'_P_mproj.tif' --out $outputPrefixFine'_P_mproj_r.tif' --int bco --OTB $OTB --nd -9999
$OTB/otbcli_BandMath -il $outputPrefixFine'_P_mproj_r.tif' $outputPrefixFine'-DEM.tif' -out $outputPrefixFine'-DEM_satmasked.tif' -exp 'im1b1>4093 ? -9999 : im2b1'
$OTB/otbcli_BandMath -il $outputPrefixFine'_P_mproj_r.tif' -out $outputPrefixFine'_sat_mask.tif' -exp 'im1b1>4093 ? 1 : 0'

echo "######################################################"
echo "###   Projection of the MS images                  ###"
echo -e "######################################################\n \n"

cd $outputFolderFine

# find nadir multi-spectral image and RPC ! correspondance between numbers and front, nadir, back varies !
inputIMG_MS=$(find $dataFolderIMG -name IMG*_MS_*006*.JP2)
inputRPC_MS=$(find $dataFolderIMG -name RPC*_MS_*006.XML)

# splits multi-layer images in several images as mapproject only handles single layer images
$OTB/otbcli_SplitImage -in $inputIMG_MS -out $outputPrefixFine'_MS.tif'

$ASP/mapproject -t rpc $DEM4proj ./$outputPrefixFine'_MS_0.tif' $inputRPC_MS ./$outputPrefixFine'_MS_red_mproj.tif'
$ASP/mapproject -t rpc $DEM4proj ./$outputPrefixFine'_MS_1.tif' $inputRPC_MS ./$outputPrefixFine'_MS_gre_mproj.tif'
$ASP/mapproject -t rpc $DEM4proj ./$outputPrefixFine'_MS_2.tif' $inputRPC_MS ./$outputPrefixFine'_MS_blu_mproj.tif'
$ASP/mapproject -t rpc $DEM4proj ./$outputPrefixFine'_MS_3.tif' $inputRPC_MS ./$outputPrefixFine'_MS_nir_mproj.tif'

# merges projected images into a single multi-spectral image
$OTB/otbcli_ConcatenateImages -il ./$outputPrefixFine'_MS_red_mproj.tif' ./$outputPrefixFine'_MS_gre_mproj.tif' ./$outputPrefixFine'_MS_blu_mproj.tif' ./$outputPrefixFine'_MS_nir_mproj.tif' -out ./$outputPrefixFine'_MS_mproj.tif'

# calculates a pan-sharpened image
$OTB/otbcli_Superimpose -inr $outputFolderFine/$outputPrefixFine'_P_mproj.tif' -inm ./$outputPrefixFine'_MS_mproj.tif' -out "./tmp.tif"
$OTB/otbcli_Pansharpening -inp $outputFolderFine/$outputPrefixFine'_P_mproj.tif' -inxs './tmp.tif' -out ./$outputPrefixFine'_PS_mproj.tif' uint16

# delete intermediate products
rm ./tmp.tif ./$outputPrefixFine'_MS_red_mproj.tif' ./$outputPrefixFine'_MS_gre_mproj.tif' ./$outputPrefixFine'_MS_blu_mproj.tif' ./$outputPrefixFine'_MS_nir_mproj.tif'

date
echo "------------ end of DEM calculation ---------------"