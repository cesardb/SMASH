# Documentation for SMASH
# 6 november 2020

# SMASH produces elevation difference map, a land surface classification and various elevation and images product from satellite stereo images (e.g. Pléiades).
 
#############################
#####   SMASH content   #####
#############################

# - SMASH_a_DEM.sh produces Digital Elevation Models (DEM), ortho-rectified panchromatic and multi-spectral images
#     IN:
#        satellite stereo images (single layer)
#        one satellite multi-spectral image
#        rational polynom coefficient files associated to each image
#        parameter file SMASH_a_DEM_init.sh
#     OUT:
#        mask of saturated pixels (*_sat_mask.tif)
#        panchromatic ortho-rectified image (*_P_mproj.tif)
#        pan-sharpened ortho-rectified image (*_PS_mproj.tif)
#        multi-spectral ortho-rectified image (*_MS_mproj.tif)
#        digital elevation model (*DEM.tif)
#        digital elevation model with saturated pixels masked (*-DEM_satmasked.tif)
#        digital elevation model without data gaps (-DEM_filled.tif)

# - SMASH_b_DDEM.sh produces elevation difference map, land surface 
#     IN:
#        elevation and image products from SMASH_a_DEM.sh
#        training vector file (e.g. .shp) for land surface classification ('label'={1:snow,2:forest,3:stable terrain,4:open water,5:snow in shade,6:forest in shade,7:stable terrain in shade})
#        parameter file SMASH_b_DDEM_init.sh
#     OUT
#        land surface classification (*classif.tif) 
#        elevation difference map (*diff.tif)

#####################################
#####   SMASH common problems   #####
#####################################

## Satellite images naming
#     Pléiades raw images name tend to vary with different acquisitions (*-1*.JP2,*-001*.JP2). Adapt L44-51 and L110-111 in SMASH_a_DEM.sh  
#     Pléiades raw images number (1,2,3) is not related to the order of acquisition. Adapt L44-51 and L110-111 in SMASH_a_DEM.sh if you want a specific order.

## ASP sparse_disp tool
# The sparse_disp option in ASP (--corr-seed-mode 3) needs some setup before use.
# See "4.5. Dealing with Terrain Lacking Large-Scale Features" in ASP doc (https://stereopipeline.readthedocs.io/en/latest/)

## tail/sed
# Depending how you run SMASH_b_DDEM.sh, L101 (shift_NnK=*) might not produce the expected result
# Try using the commented line.