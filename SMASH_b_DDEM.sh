#!/bin/bash
echo "###########################################"
echo "###          STARTING SMASH             ###"
echo "###       SNOW DEPTH PRODUCTION         ###"
echo -e "###########################################\n \n"
date

echo "##################################################################"
echo "############            Read Parameters              #############"
echo -e "##################################################################\n \n"

source mySMASHtoolbox.sh
source SMASH_b_DEM_init.sh

echo "##################################################################"
echo "############                Summer data               ############"
echo -e "##################################################################\n \n"

summerFolder=$outputFolderDEM'/PL_'$dateSummer'_GSD'
"/PL_"$DATE"_GSD"$GSDfine
summerDEM=$(find $summerFolder -wholename '*/*GSD'$GSDfine'-DEM.tif')

echo "##################################################################"
echo "############                Winter data               ############"
echo -e "##################################################################\n \n"

for dateIm in ${dateList[@]}
do
echo 'Treating data '$dateIm
prodFolder=$outputFolderDEM'/PL_'$dateIm'_GSD'$GSDfine
outFolderClass=$outputFolderSNOWDEPTH0'/class/PL_'$dateIm
outFolderClassReca=$outputFolderSNOWDEPTH0'/class_reca/PL_'$dateIm
outFolderDiff=$outputFolderSNOWDEPTH0'/diff_NnK/PL_'$dateIm
outFolderReca=$outputFolderSNOWDEPTH0'/reca_NnK/PL_'$dateIm

mkdir -p $outFolderClass $outFolderClassReca $outFolderDiff $outFolderReca

inDEM=$(find $prodFolder -wholename '*/*GSD'$GSDfine'-DEM_satmasked.tif')

echo "#####################################################################"
echo "##########     Image preparation for classif calculation      #######"
echo -e "#####################################################################\n \n"

# input image (ortho-rectified multi-spectral)
inIm=$(find $prodFolder -wholename '*/*MS_mproj.tif')

$OTB/otbcli_BandMath -il $inIm -exp " im1b1==-32768 ? 0:1 " -out $outFolderClass"/tmpND_"${dateIm}".tif"
$OTB/otbcli_RadiometricIndices -in $inIm -channels.red 1 -channels.green 2 -channels.blue 3 -channels.nir 4  -list Vegetation:NDVI -out $outFolderClass"/"${dateIm}"_GSD"$GSDfine"_classif_Sup_rf_MS_RadInd.tif"
$OTB/otbcli_ConcatenateImages -il $inIm $outFolderClass"/"${dateIm}"_GSD"$GSDfine"_classif_Sup_rf_MS_RadInd.tif" -out $outFolderClass"/im4classif_radio_"$dateIm".tif"
$OTB/otbcli_ManageNoData -in $outFolderClass"/im4classif_radio_"$dateIm".tif" -mode changevalue -mode.changevalue.newv -9999 -out $outFolderClass"/im4classif_radio_"$dateIm".tif"
$OTB/otbcli_ManageNoData -in $outFolderClass"/im4classif_radio_"$dateIm".tif" -mode apply -mode.apply.mask $outFolderClass"/tmpND_"${dateIm}".tif" -mode.apply.ndval -9999 -out $outFolderClass"/im4classif_radio_"$dateIm".tif"

echo "####################################################################"
echo "#########         Classif calculation         ######################"
echo -e "####################################################################\n \n"

# input image (R+G+B+NDVI) 
inIm=$(find $outFolderClass -wholename '*/im4classif_radio_*.tif')
# training vector
trainSHP=$trainFolder"/training_poly_"$dateIm".shp"

$OTB/otbcli_PolygonClassStatistics -in $inIm -vec $trainSHP -field label -out $outFolderClass"/classes.xml"
$OTB/otbcli_SampleSelection -in  $inIm -vec $trainSHP -instats $outFolderClass"/classes.xml" -field label -strategy all -outrates $outFolderClass"/rates.csv" -out $outFolderClass"/samples.sqlite"
$OTB/otbcli_SampleExtraction -in $inIm -vec $outFolderClass"/samples.sqlite" -outfield prefix -outfield.prefix.name band_ -field label
$OTB/otbcli_TrainVectorClassifier -io.vd $outFolderClass"/samples.sqlite"  -cfield label -io.out $outFolderClass/${dateIm}"_model.rf" -classifier sharkrf -feat band_0 band_1 band_2 band_3 band_4     # c'est ici que ca se joue les primitives choisies        
$OTB/otbcli_ImageClassifier  -in $inIm -model  $outFolderClass/${dateIm}"_model.rf" -out $outFolderClass/${dateIm}"_GSD"$GSDfine"_classif_Sup_rf_MS_class4.tif" -nodatalabel -9999
$OTB/otbcli_ManageNoData -in $outFolderClass/${dateIm}"_GSD"$GSDfine"_classif_Sup_rf_MS_class4.tif" -out  $outFolderClass/${dateIm}"_GSD"$GSDfine"_classif_Sup_rf_MS_class4.tif" -mode apply -mode.apply.mask $outFolderClass"/tmpND_"${dateIm}".tif"
$OTB/otbcli_BandMath -il $inIm $outFolderClass/${dateIm}"_GSD"$GSDfine"_classif_Sup_rf_MS_class4.tif" -out $outFolderClass/${dateIm}"_classif.tif" -exp " ((im1b1+im1b2+im1b3)/3>2000) ? 1 : im2b1 "
rm -f $outFolderClass"/im4classif_radio_"$dateIm".tif" $outFolderClass"/"${dateIm}"_GSD"$GSDfine"_classif_Sup_rf_MS_RadInd.tif" $outFolderClass"/tmpND_"${dateIm}".tif"

echo "####################################################################"
echo "############         Mask calculation         ######################"
echo -e "####################################################################\n \n"

# morphological erosion parameter
sizeSnow=2
sizeStable=2

inIm=$(find $outFolderClass -wholename '*/*_classif.tif')

# create snow mask
myMakeMaskFromClassif  --OTB $OTB --inClassif $inIm --outName $outFolderClass/$dateIm"_snowmask.tif" --value 1 --sizeer $sizeSnow --sizesie 30
myMakeMaskFromClassif  --OTB $OTB --inClassif $inIm --outName $outFolderClass/$dateIm"_snowshademask.tif" --value 5 --sizeer $sizeSnow --sizesie 30

## create forest mask
myMakeMaskFromClassif  --OTB $OTB --inClassif $inIm --outName $outFolderClass/$dateIm"_forestmask.tif" --value 2 --sizeer $sizeSnow --sizesie 30
myMakeMaskFromClassif  --OTB $OTB --inClassif $inIm --outName $outFolderClass/$dateIm"_forestshademask.tif" --value 6 --sizeer $sizeSnow --sizesie 30

## create stable mask
myMakeMaskFromClassif  --OTB $OTB --inClassif $inIm --outName $outFolderClass/$dateIm"_stablemask.tif" --value 3 --sizeer $sizeStable --sizesie 30
myMakeMaskFromClassif  --OTB $OTB --inClassif $inIm --outName $outFolderClass/$dateIm"_stableshademask.tif" --value 7 --sizeer $sizeStable --sizesie 30

## create lake mask
myMakeMaskFromClassif  --OTB $OTB --inClassif $inIm --outName $outFolderClass/$dateIm"_lakemask.tif" --value 4 --sizeer $sizeStable --sizesie 30

echo "####################################################################"
echo "############          Co-registration         ######################"
echo -e "####################################################################\n \n"

# co-register winter DEM to summer DEM
python RecaNuthKaab_201018.py -DEMmob $inDEM -DEMref $summerDEM -MSKname  $outFolderClass/$dateIm"_stablemask.tif" -thresholdNMAD 0.5 -thresholdXYvec 0.05 -Nmax 10 -maxSLP 45 -minSLP 4 -nNMAD 6 -outDir $outFolderReca/ -suffix _reca2s -OTB $OTB -MeanMed median -tilt 0 -Zcor 1 -maskSTB 1

# shifts other products according to shift determined with RecaNuthKaab_200312.py
#shift_NnK=( $(cut -d',' -f1 <<< $(tail -n 1 $outFolderReca/log*)) $(cut -d',' -f2 <<< $(tail -n 1 $outFolderReca/log*)) ) : can be tried if line below provides weird outputs
shift_NnK=( $(cut -d',' -f1 <<< $(sed -n 2p $outFolderReca/log*)) $(cut -d',' -f2 <<< $(sed -n 2p $outFolderReca/log*)) )

ImList=$(find $outFolderClass -wholename '*/*.tif')
for inIm in $ImList
do
   tmp=$(basename $inIm)
   myrecaIMG  --inIMG $inIm --outIMG $outFolderClassReca/$tmp --dX ${shift_NnK[0]} --dY ${shift_NnK[1]} --dZ 0 --inv true
done

inIm=$(find $prodFolder -wholename '*/*MS_mproj.tif')
myrecaIMG  --inIMG $inIm --outIMG $outFolderClassReca/$dateIm'_MS_mproj.tif' --dX ${shift_NnK[0]} --dY ${shift_NnK[1]} --dZ 0 --inv true

inIm=$(find $prodFolder -wholename '*/*P_mproj.tif')
myrecaIMG  --inIMG $inIm --outIMG $outFolderClassReca/$dateIm'_P_mproj.tif' --dX ${shift_NnK[0]} --dY ${shift_NnK[1]} --dZ 0 --inv true

inIm=$(find $prodFolder -wholename '*/*sat_mask.tif')
myrecaIMG  --inIMG $inIm --outIMG $outFolderClassReca/$dateIm'_sat_mask.tif' --dX ${shift_NnK[0]} --dY ${shift_NnK[1]} --dZ 0 --inv true

echo "###################################################################"
echo "##########          dH map calculation         ####################"
echo -e "###################################################################\n \n"

## differenciation
$OTB/otbcli_BandMath -il $outFolderReca"/"*reca2s.tif $summerDEM -out $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff.tif' -exp "im1b1-im2b1"
$OTB/otbcli_BandMath -il $summerDEM $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff.tif' -out $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff.tif' -exp "im1b1==-9999 ? -9999:im2b1"
$OTB/otbcli_BandMath -il $outFolderReca"/"*reca2s.tif $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff.tif' -out $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff.tif' -exp "im1b1==-9999 ? -9999:im2b1"

# differenciation with jitter correction
$OTB/otbcli_BandMath -il $outFolderReca"/"*reca2s_jitter_det.tif $summerDEM -out $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff_jitter_det.tif' -exp "im1b1-im2b1"
$OTB/otbcli_BandMath -il $summerDEM $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff_jitter_det.tif' -out $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff_jitter_det.tif' -exp "im1b1==-9999 ? -9999:im2b1"
$OTB/otbcli_BandMath -il $outFolderReca"/"*reca2s_jitter_det.tif $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff_jitter_det.tif' -out $outFolderDiff'/dH_'${dateIm}'_'${dateSummer}'_NnK-diff_jitter_det.tif' -exp "im1b1==-9999 ? -9999:im2b1"

# set various products to the same grid
mySuperImposeOTB --IMGref $summerDEM --IMGmob $outFolderClassReca'/'$dateIm'_P_mproj.tif' --out $outFolderReca'/'$dateIm'_P_mproj_resh.tif' --int bco --OTB $OTB --nd -9999
mySuperImposeOTB --IMGref $summerDEM --IMGmob $outFolderClassReca'/'$dateIm'_forestmask.tif' --out $outFolderClassReca'/'$dateIm'_forestmask_resh.tif' --int nn --OTB $OTB --nd -9999
mySuperImposeOTB --IMGref $summerDEM --IMGmob $outFolderClassReca'/'$dateIm'_snowmask.tif' --out $outFolderClassReca'/'$dateIm'_snowmask_resh.tif' --int nn --OTB $OTB --nd -9999
mySuperImposeOTB --IMGref $summerDEM --IMGmob $outFolderClassReca'/'$dateIm'_stablemask.tif' --out $outFolderClassReca'/'$dateIm'_stablemask_resh.tif' --int nn --OTB $OTB --nd -9999
mySuperImposeOTB --IMGref $summerDEM --IMGmob $outFolderClassReca'/'$dateIm'_classif.tif' --out  $outFolderClassReca'/'$dateIm'_classif_resh.tif' --int nn --OTB $OTB --nd -9999
mySuperImposeOTB --IMGref $summerDEM --IMGmob $outFolderClassReca'/'$dateIm'_sat_mask.tif' --out  $outFolderClassReca'/'$dateIm'_sat_mask.tif' --int nn --OTB $OTB --nd -9999
done

#### Zip what matters
cd $outputFolderSNOWDEPTH0
zip $zipFolder'/'$exp'_classif.zip' ./class_reca/*/*classif_resh.tif
zip $zipFolder'/'$exp'_mask.zip' ./class_reca/*/*mask_resh.tif
zip $zipFolder'/'$exp'_diff.zip' ./diff_NnK/*/*.tif

echo "------------ end of dDEM calculation ---------------"
