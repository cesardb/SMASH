#!/usr/bin/python
#-*- coding: utf-8 -*-

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
from osgeo import osr, gdal
import scipy.interpolate
import scipy.optimize as optimization
import os
import argparse
import csv
import shutil
from scipy.fftpack import fft, ifft, fftfreq

np.warnings.filterwarnings('ignore')

def f(x, a, b, c):
    y = a * np.cos(np.deg2rad(b-x))  + c
    return y
    
def f_tilt(x, a, b):
    y = (a*x)+b
    return y

def nmad(Z):
    return 1.4826*np.nanmedian(abs(Z-np.nanmedian(Z)))

def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())

def mymode(data,limit):
    data[abs(data)>limit]=np.nan
    hist,bins=np.histogram(data,bins=limit*200,range=(-limit,limit))
    nmax=np.where(hist==np.max(hist))[0][0]
    mode=(bins[nmax]+bins[nmax+1])/2
    return mode
    
def mygetRSTdata(inRST):
    print('\nImporting data from raster: '+inRST)
    ds = gdal.Open(inRST)
    Band = ds.GetRasterBand(1)
    RST = Band.ReadAsArray()
    RST = np.flip( RST, 0)
    RST = RST.astype('float32')
    if Band.GetNoDataValue()==None:
        print('! Warning ! No data value is empty for '+inRST+'. Masking might not work properly.')
        mask = np.ones(np.shape(RST)).astype(bool)
    else:
        mask = ((RST != Band.GetNoDataValue()))
    Band=None
    ds = None
    return RST,mask
    
def mygetRSTgrid(inRST):
    print('\nImporting grid from : '+inRST)
    ds = gdal.Open(inRST)
    x0, dx, dxdy, y0, dydx, dy = ds.GetGeoTransform()
    XSize = ds.RasterXSize
    YSize = ds.RasterYSize
    x1 = x0 + XSize*dx
    y1 = y0 + YSize*dy
    X = np.arange(x0,x1,dx)
    Y = np.arange(y0,y1,dy)
    Y = np.flip(Y,0)
    return X,Y
    
def myFitTilt(dH,f_tilt, outDir = None):
    ### check for tilt
    print('--------------')
    print('Treating tilt')
    # in the y direction
    print('Treating tilt in y direction')
    lx = np.shape(dH)[1]
    ly = np.shape(dH)[0]    
    
    dZy = np.nanmedian(dH,axis = 1)
    wy = np.where((np.isnan(dZy) == 0) & (np.sum(np.isnan(dH) == 0,1)>20 ))
    fitdZy = optimization.curve_fit(f_tilt, wy[0], dZy[wy[0]],method = 'trf')[0]
    
    dTilty1D = fitdZy[1]+fitdZy[0]*np.arange(ly)
    if abs(dTilty1D[-1]-dTilty1D[0])>0.1:
        dTilty = np.sum(np.meshgrid(0*np.arange(lx),dTilty1D),axis = 0)    
    else:
        dTilty = np.full_like(dH,0)
    dH = dH-dTilty
    
    # in the x direction
    print('Treating tilt in x direction')
    dZx = np.nanmedian(dH,axis = 0)
    wx = np.where((np.isnan(dZx) == 0) & (np.sum(np.isnan(dH) == 0,0)>20 ))
    fitdZx = optimization.curve_fit(f_tilt, wx[0], dZx[wx[0]],method = 'trf')[0]
    
    dTiltx1D = fitdZx[1]+fitdZx[0]*np.arange(len(dZx))
    if abs(dTiltx1D[-1]-dTiltx1D[0])>0.05:
        dTiltx = np.sum(np.meshgrid(dTiltx1D,0*np.arange(ly)),axis = 0)
    else:
        dTiltx = np.full_like(dH,0)
    dH = dH-dTiltx
    
    dTilt = dTilty+dTiltx
    
    if outDir:
        plt.figure()
        plt.subplot(211)
        plt.plot(wx[0],dZx[wx[0]],'r.')
        plt.plot(np.arange(lx),dTiltx1D,'k')
        plt.subplot(212)
        plt.plot(wy[0],dZy[wy[0]],'r.')
        plt.plot(np.arange(ly),dTilty1D,'k')
        plt.savefig(outDir+'/Tilt_corection.svg')
    return dTilt
    
def myCleanDEM(DEM, nd):
    outDEM = np.copy(DEM)
    # from each side, remove lines which are identical
    mean0 = np.nanmean(DEM,0)
    mean1 = np.nanmean(DEM,1)

    std0 = np.nanstd(DEM,0)
    std1 = np.nanstd(DEM,1)
        
    # check if lines are identical from left
    nmean = np.where(mean0!= mean0[0])[0][0]
    nstd = np.where(std0!= std0[0])[0][0]
    
    if nmean == nstd:
        if nmean>1:
            outDEM[:,0:nmean-1] = nd


    # check if lines are identical from top
    nmean = np.where(mean1!= mean1[0])[0][0]
    nstd = np.where(std1!= std1[0])[0][0]
    
    if nmean == nstd:
        if nmean>1:
            outDEM[0:nmean-1,:] = nd
    
    # check if lines are identical from right
    nmean = np.where(np.flip(mean0,0)!= mean0[-1])[0][0]
    nstd = np.where(np.flip(std0,0)!= std0[-1])[0][0]

    if nmean == nstd:
        if nmean>1:
            outDEM[:,-nmean+1::] = nd


    # check if lines are identical from bottom
    nmean = np.where(np.flip(mean1,0)!= mean1[-1])[0][0]
    nstd = np.where(np.flip(std1,0)!= std1[-1])[0][0]
    
    if nmean == nstd:
        if nmean>1:
            outDEM[-nmean+1::,:] = nd
    return outDEM
    
def mygetNnK_dXdY(dH,SLP,ASP,Reca0,minSLP):
    xxdata = ASP.flatten()
    yydata = dH.flatten()/np.tan(np.deg2rad(SLP.flatten()))
    yydata[SLP.flatten()<minSLP] = np.float32(np.nan)
    fnan = ~np.isnan(yydata)
    xxdata = xxdata[fnan]
    yydata = yydata[fnan]
            
    xdata = []
    ydata = []
    step_asp = 5.
    for asp in np.arange(0,360,step_asp):
        xdata.append(asp+step_asp/2)
        ydata.append(np.median(yydata[(xxdata>asp) & (xxdata<asp+step_asp)]))
    xdata = np.array(xdata)
    ydata = np.array(ydata)
    m1 = ((xdata!= 0))
    m2 = ((ydata!= 0))
            
    mask_nan = m1*m2
    fit, pcov = optimization.curve_fit(f, xdata[mask_nan], ydata[mask_nan] ,Reca0,method = 'trf') #[0] #method : {‘lm’, ‘trf’, ‘dogbox’},
    
    plt.figure(1)
    plt.plot(np.arange(0.5,360.5,step_asp),ydata-f(np.arange(0.5,360.5,step_asp), fit[0], fit[1], fit[2]), 'g')
                        
    # ! Calculate dX,dY ! #
    dX = np.round(fit[0]*np.sin(np.deg2rad(fit[1])),4)
    dY = np.round(fit[0]*np.cos(np.deg2rad(fit[1])),4)
    Reca0 = np.array([fit[0],fit[1], np.median(ydata[~np.isnan(ydata)])])
    return dX,dY,Reca0
    
def myget_dH(DEMm,DEMr,mask,nNMAD):
    dH = DEMm-DEMr
    dH = np.multiply(dH,mask.astype('float32'))
    if nNMAD is not None:
        dH[abs(dH-np.nanmedian(dH))-nNMAD*nmad(dH.flatten())>0] = np.float32(np.nan)
    return dH
    
def myget_dZ(dH,MeanMed,mask_Z):
    if MeanMed == "mean":
        dZ = np.round(np.nanmean(dH[mask_Z==1].flatten()),4)
    elif MeanMed == "median":
        dZ = np.round(np.nanmedian(dH[mask_Z==1].flatten()),4)
    elif MeanMed == 'mode':
        dZ = np.round(mymode(dH[mask_Z==1].flatten(),10),4)
    return dZ

#to be checked : how are treated the NaN/NoData
def myRectBivariateSplite(Ym, Xm, Zm, Yr, Xr):
    # Raster Zm on the grid Ym, Xm is interpolated into the grid Yr, Xr.
    fint = scipy.interpolate.RectBivariateSpline(Ym,Xm,Zm,kx = 1, ky = 1)
    Z = fint( Yr, Xr )
    return Z

def myNNGridInterp(Ym, Xm, Zm, Yr, Xr):
    # Raster Zm on the grid Ym, Xm is interpolated into the grid Yr, Xr.
    fint = scipy.interpolate.RegularGridInterpolator( (Ym,Xm),Zm, method="nearest",bounds_error=False, fill_value=np.nan)
    Z = fint( Yr, Xr )
    return Z
    
def myapply_dXdY(Xm,Ym,Z0,Xr,Yr,dX,dY,maskm2int0,tmp_nd,typ_Z_val):
    X0 = Xm-dX
    Y0 = Ym-dY
    
    # ! Update DEMm for dX,dY ! #
    Z = myRectBivariateSplite(Y0, X0, Z0, Yr, Xr)
    maskm2int_mv = myRectBivariateSplite(Y0, X0, maskm2int0, Yr, Xr)
    maskm2int_mv = np.round(maskm2int_mv,5) #4) # avoids a weird bug in python which 
    
    Z[ maskm2int_mv != typ_Z_val ] = tmp_nd #nd
    return Z

def myapply_dXdY_NN(Xm,Ym,Z0,Xr,Yr,dX,dY):
    X0 = Xm-dX
    Y0 = Ym-dY
    
    # ! Update DEMm for dX,dY ! #
    Z = myRectBivariateSplite(Y0, X0, Z0, Yr, Xr)
    Z = (Z>0.5)
    #maskm2int_mv = myRectBivariateSplite(Y0, X0, maskm2int0, Yr, Xr)
    #maskm2int_mv = np.round(maskm2int_mv,4) #5) # avoids a weird bug in python which 
    #Z[ maskm2int_mv != typ_Z_val ] = tmp_nd #nd
    return Z

def myapply_dZ_2D(DEM,dTilt):
    outDEM=DEM-dTilt
    return outDEM    

def myapply_dZ(DEM,dZ):
    outDEM=DEM-dZ
    return outDEM    

def mygetStat_dH(dH,Reca_Stat):
    Reca_Stat['MEAN_resi'].append(np.round(np.nanmean(dH.flatten()),3))
    Reca_Stat['MEDIAN_resi'].append(np.round(np.nanmedian(dH.flatten()),3))
    Reca_Stat['STD_resi'].append(np.round(np.nanstd(dH.flatten()),3))
    Reca_Stat['NMAD_resi'].append(np.round(nmad(dH.flatten()),3))
    return Reca_Stat

def myTestRECA(NMAD_resi,thresholdNMAD,N,Nmax,thresholdXYvec,dX,dY):
    # CONDITION ON NMAD
    Improvement_NMAD_abs=np.round(NMAD_resi[-1]-NMAD_resi[-2],4)
    Improvement_NMAD_pct=np.round((100*((Improvement_NMAD_abs)/NMAD_resi[-2])),3)
    if N>1:
        testNMAD = ( np.abs(Improvement_NMAD_pct)> thresholdNMAD )
    else:
        testNMAD = True
    testN = ( N < Nmax )
    
    if (np.sqrt(np.square(dX)+np.square(dY)) > thresholdXYvec) and (dX!=0) and (dY!=0):
        testXYvec=True
    else:
        testXYvec=False
    
    print('dX is :'+str(dX))
    print('dY is :'+str(dY))
    print('testXY is : '+str(testXYvec))
    print('testNMAD is : '+str(testNMAD))
    print('testN is : '+str(testN))
    testRECA = testNMAD*testN*testXYvec
    
    if Improvement_NMAD_pct<0:
        print('This loop improved NMAD by '+str(np.abs(Improvement_NMAD_pct))+' % or '+str(Improvement_NMAD_abs)+' m (previous NMAD :'+str(NMAD_resi[-2])+' m, current NMAD : '+str(NMAD_resi[-1])+' m)')
    else:
        print('WARNING This loop degraded NMAD by '+str(np.abs(Improvement_NMAD_pct))+' % or '+str(Improvement_NMAD_abs)+' m (previous NMAD :'+str(NMAD_resi[-2])+' m, current NMAD : '+str(NMAD_resi[-1])+' m)') 
    return testRECA

def mymakeFig_dH(dH,Xr,Yr,outDir,suffix):
    # view dH map
    plt.figure()
    plt.imshow(dH,origin='lower',extent=(Xr[0],Xr[-1],Yr[0],Yr[-1]),cmap='RdYlBu',clim=(-1,1))
    plt.colorbar()
    plt.savefig(outDir+'/dH_map_'+suffix+'.svg')
    plt.close()
    
    # view along axis
    plt.figure()
    plt.plot(Xr,np.nanmedian(dH,axis=0))
    plt.grid(True)
    plt.hlines(0,plt.gca().get_xlim()[0],plt.gca().get_xlim()[1])
    plt.title('Median per column (m)')  
    plt.savefig(outDir+'/dH_alongx_'+suffix+'.svg')
    plt.close()
    
    plt.figure()
    plt.plot(Yr,np.nanmedian(dH,axis=1))
    plt.grid(True)
    plt.hlines(0,plt.gca().get_xlim()[0],plt.gca().get_xlim()[1])
    plt.title('Median per line (m)')    
    plt.savefig(outDir+'/dH_alongy_'+suffix+'.svg')
    plt.close()
    
    plt.figure()
    plt.hist(dH.flatten(),bins=200,range=(-2,2),histtype='step')
    plt.vlines(np.nanmean(dH.flatten()),plt.gca().get_ylim()[0],plt.gca().get_ylim()[1],'r')
    plt.vlines(np.nanmedian(dH.flatten()),plt.gca().get_ylim()[0],plt.gca().get_ylim()[1],'g')
    plt.vlines(mymode(dH.flatten(),10),plt.gca().get_ylim()[0],plt.gca().get_ylim()[1],'b')
    plt.vlines(0,plt.gca().get_ylim()[0],plt.gca().get_ylim()[1],'k')
    plt.grid(True)
    plt.savefig(outDir+'/dH_hist_'+suffix+'.svg')
    plt.close()
    
def RecaNuthKaab_201028( DEMmob = None, DEMref = None, SLPrst = None, ASPrst = None, MSKname = None, MSKZname = None, thresholdNMAD = 1, thresholdXYvec=0.1, Nmax = 5, minSLP = 1, maxSLP = 90, nNMAD = None, outDir = None, suffix = None, OTB = None, MeanMed = "median", tilt = 0,Zcor = 1,maskSTB=1 ):

    print('\n START OF CO-REGISTRATRION \n')
    if not os.path.exists(outDir):
        os.mkdir(outDir)
 
    ########################################
    ########## Initiate variables ##########
    ########################################   
    testRECA = 1
    dZ=0
    Reca_Vec={'dX':[],'dY':[],'dZ':[]}
    Reca_Stat={'MEAN_resi':[],'MEDIAN_resi':[],'STD_resi':[],'NMAD_resi':[]}
    Reca0 = np.array((1,1,0))
    MeanMed = str(MeanMed)

    
    ########################################
    ##########     Open data     ###########
    ########################################      
    
    # Open DEM ref
    print('\nWorking on DEM reference : '+DEMref)
    DEMr0,mask_ND=mygetRSTdata(DEMref)
    Xr,Yr=mygetRSTgrid(DEMref)
    DEMr=DEMr0.astype('float32')
    DEMr0=None 
    
    # Open DEM mob    
    print('\nWorking on DEM mobile : '+DEMmob)
    DEMm0,maskm=mygetRSTdata(DEMmob)
    Xm,Ym=mygetRSTgrid(DEMmob)
    
    tmp_nd=int(np.nanmean(DEMm0*maskm))-200
    typ_Z_val=int(np.nanmean(DEMm0*maskm))
    maskm2int0 = np.copy(maskm)    
    maskm2int0 = maskm2int0.astype('float32')
    maskm2int0[maskm2int0 == 1] = typ_Z_val
    maskm2int0[maskm2int0 == 0] = tmp_nd
    DEMmv=np.copy(DEMm0)
    DEMmv[ maskm2int0 == tmp_nd ] = tmp_nd #nd
    Z0 = np.copy(DEMmv)

    
    # set DEMmob and DEMref to the same grid if necessary
    if not np.array_equal(Xm, Xr) or not np.array_equal(Ym, Yr): 
        print('! Warning ! DEMmob and DEMref do not have the same grid: reprojecting.')# DEMmob is interpolated into DEMref grid.' )
        DEMmv = myRectBivariateSplite(Ym, Xm, DEMm0, Yr, Xr)
        maskm2int_mv = myRectBivariateSplite(Ym, Xm, maskm2int0, Yr, Xr)

        maskm2int_mv = np.round(maskm2int_mv,4) # avoids a weird bug in python which 
        DEMmv[ maskm2int_mv != typ_Z_val ] = tmp_nd #nd
        mask_ND = mask_ND*(maskm2int_mv == typ_Z_val )
    else:
        mask_ND=mask_ND*maskm
    maskm=None
    DEMm=DEMmv.astype('float32')
    DEMmv=None
    
    # Calculate slope if not provided
    if SLPrst is None:
        print('Calculating slope. Writting in '+outDir+'/tmp-SLP.tif')
        os.system('gdaldem slope  '+DEMref+' '+outDir+'/tmp-SLP.tif')
        SLPrst = outDir+'/tmp-SLP.tif'
    
    # Calculate aspect if not provided
    if ASPrst is None:
        print('Calculating aspect. Writting in '+outDir+'/tmp-ASP.tif')
        os.system('gdaldem aspect  '+DEMref+' '+outDir+'/tmp-ASP.tif')
        ASPrst = outDir+'/tmp-ASP.tif'
        
    # Open slope raster
    print('\nWorking on slope raster : '+SLPrst)
    SLP0,mask_tmp=mygetRSTdata(SLPrst)
    mask_ND = mask_ND*mask_tmp #((SLP0!= Band.GetNoDataValue()))
    # mask too steep slopes
    mask_ND = mask_ND*((SLP0<maxSLP))
    SLP = SLP0.astype('float32')
    SLP0=None
    

    # Open aspect raster
    print('\nWorking on aspect raster : '+ASPrst)
    ASP0,mask_tmp=mygetRSTdata(ASPrst)
    mask_ND = mask_ND*mask_tmp #((ASP0!= Band.GetNoDataValue()))
    mask_tmp=None
    ASP = ASP0.astype('float32')
    ASP0=None
    
    # open stable mask raster or create it from shp if necessary 
    if MSKname is not None:
        if os.path.splitext(MSKname)[1] == '.shp' or os.path.splitext(MSKname)[1] == '.SHP' :
            os.system(OTB+'/otbcli_Rasterization -in '+MSKname+' -im '+DEMref+' -out '+outDir+'/tmp-mask.tif -mode binary -mode.binary.foreground 1')
            MSKrst = outDir+'/tmp-mask.tif'
        else:
            MSKrst = MSKname
        print('Opening Mask raster : '+MSKrst)
        mask_STB0,mask_tmp=mygetRSTdata(MSKrst)
        Xmask,Ymask=mygetRSTgrid(MSKrst)

        if not np.array_equal(Xmask, Xr) or not np.array_equal(Ymask, Yr):
            print('! Warning ! Mask and DEMref do not have the same grid: reprojecting.')# DEMmob is interpolated into DEMref grid.' )
            os.system('gdalwarp -overwrite -tr '+str(Xr[1]-Xr[0])+' '+str(Yr[0]-Yr[1])+' -te '+str(Xr[0])+' '+str(Yr[0])+' '+str(Xr[-1]+Xr[1]-Xr[0])+' '+str(Yr[-1]+Yr[1]-Yr[0])+' -r near -dstnodata -9999 '+MSKrst+' '+outDir+'/tmp-mask.tif')
            MSKrst = outDir+'/tmp-mask.tif'
            print('Opening Mask raster : '+MSKrst)
            mask_STB0,mask_tmp=mygetRSTdata(MSKrst)                 
            mask_ND = mask_ND*mask_tmp
            mask_tmp=None           
        else:
            mask_ND = mask_ND*mask_tmp
            mask_tmp=None
    else:
        mask_STB0=np.full_like(mask_ND,1)

    # open Z mask 
    if MSKZname is not None:
        if os.path.splitext(MSKZname)[1] == '.shp' or os.path.splitext(MSKZname)[1] == '.SHP' :
            os.system(OTB+'/otbcli_Rasterization -in '+MSKZname+' -im '+DEMref+' -out '+outDir+'/tmp-mask.tif -mode binary -mode.binary.foreground 1')
            MSKrst = outDir+'/tmp-mask.tif'
        else:
            MSKrst = MSKZname
        print('Opening Mask raster : '+MSKrst)
        mask_Z0,mask_tmp=mygetRSTdata(MSKrst)
        Xmask,Ymask=mygetRSTgrid(MSKrst)

        if not np.array_equal(Xmask, Xr) or not np.array_equal(Ymask, Yr):
            print('! Warning ! Mask and DEMref do not have the same grid: reprojecting.')# DEMmob is interpolated into DEMref grid.' )
            os.system('gdalwarp -overwrite -tr '+str(Xr[1]-Xr[0])+' '+str(Yr[0]-Yr[1])+' -te '+str(Xr[0])+' '+str(Yr[0])+' '+str(Xr[-1]+Xr[1]-Xr[0])+' '+str(Yr[-1]+Yr[1]-Yr[0])+' -r near -dstnodata -9999 '+MSKrst+' '+outDir+'/tmp-mask_Z.tif')
            MSKrst = outDir+'/tmp-mask_Z.tif'
            print('Opening Mask raster : '+MSKrst)
            mask_Z0,mask_tmp=mygetRSTdata(MSKrst)                 
            mask_tmp=None           
        else:
            mask_tmp=None
    else:
        mask_Z0=np.full_like(mask_ND,1)

    # set mask to the DEMref grid if necessary
    if maskSTB:
        mask=mask_ND*mask_STB0
    else:
        mask=mask_ND
    
    # Mask the raster with all the rasters no-data
    mask = mask.astype('float32')
    mask[mask == 0] = np.nan
    
    # calculate dH
    dH = myget_dH(DEMm,DEMr,mask,nNMAD)
    mymakeFig_dH(dH,Xr,Yr,outDir,'initial')    
    Reca_Stat=mygetStat_dH(dH,Reca_Stat)

    Nloop=0
    while testRECA:
        Nloop+=1        
        print('-----------------------------')
        print('Iteration number '+str(Nloop))
        
        # calculate dX,dY
        dX,dY,Reca0 = mygetNnK_dXdY(dH,SLP,ASP,Reca0,minSLP)
        Reca_Vec['dX'].append(dX)
        Reca_Vec['dY'].append(dY)
        
        # apply dX,dY to DEMm and maskSTB
        DEMm = myapply_dXdY(Xm,Ym,Z0,Xr,Yr,np.sum(Reca_Vec['dX']),np.sum(Reca_Vec['dY']),maskm2int0,tmp_nd,typ_Z_val)
        
        if maskSTB:        
            mask_STB = myapply_dXdY_NN(Xr,Yr,mask_STB0,Xr,Yr,np.sum(Reca_Vec['dX']),np.sum(Reca_Vec['dY']))
            mask=mask_ND*mask_STB
        else:
            mask=mask_ND        
        mask = mask.astype('float32')
        mask[mask == 0] = np.nan
        
        # move Z mask
        mask_Z = myapply_dXdY_NN(Xr,Yr,mask_Z0,Xr,Yr,np.sum(Reca_Vec['dX']),np.sum(Reca_Vec['dY']))

        # calculate dH
        dH = myget_dH(DEMm,DEMr,mask,nNMAD)
    
        if Zcor:
            
            # calculate dZ from dH
            dZ=dZ+myget_dZ(dH,MeanMed,mask_Z)
                
            # apply dZ to DEMm
            DEMm=myapply_dZ(DEMm,dZ)
        
            # calculate dH
            dH = myget_dH(DEMm,DEMr,mask,nNMAD)
        
        if tilt:
            # calculate dH
            dH = myget_dH(DEMm,DEMr,mask,nNMAD)
    
            # calculate dTilt
            dTilt = myFitTilt(dH,f_tilt, outDir = None)
    
            # apply dTilt to DEMm
            DEMm=myapply_dZ_2D(DEMm,dTilt)
            
            # calculate dH
            dH = myget_dH(DEMm,DEMr,mask,nNMAD)
    
            if Zcor:
                # calculate dZ from dH
                dZt=myget_dZ(dH,MeanMed,mask_Z)
                    
                # apply dZ to DEMm
                DEMm=myapply_dZ(DEMm,dZt)
                dZ=dZ+dZt
                # calculate dH
                dH = myget_dH(DEMm,DEMr,mask,nNMAD)
        else:
            dTilt=np.array([[0,0],[0,0]])
            
            
        Reca_Stat=mygetStat_dH(dH,Reca_Stat)
        
        print('dZ is: '+str(dZ))
        Reca_Vec['dZ'].append(dZ)
        dZ=0
        
        testRECA=myTestRECA(Reca_Stat['NMAD_resi'],thresholdNMAD,Nloop,Nmax,thresholdXYvec,dX,dY)
    
    
    # Final shift
    # Verify if the coreg did not degrade the NMAD!
    if (Reca_Stat['NMAD_resi'][-1]-Reca_Stat['NMAD_resi'][0])/Reca_Stat['NMAD_resi'][0] > 0.0:
        print('WARNING: NMAD was degraded during this co-registration. [dX,dY] is set to [0,0].')
        Reca_Vec['dX']=0
        Reca_Vec['dY']=0
    DEMm = myapply_dXdY(Xm,Ym,Z0,Xr,Yr,np.sum(Reca_Vec['dX']),np.sum(Reca_Vec['dY']),maskm2int0,tmp_nd,typ_Z_val)

    if maskSTB:        
        mask_STB = myapply_dXdY_NN(Xr,Yr,mask_STB0,Xr,Yr,np.sum(Reca_Vec['dX']),np.sum(Reca_Vec['dY']))
        mask=mask_ND*mask_STB
    else:
        mask=mask_ND        
    mask = mask.astype('float32')
    mask[mask == 0] = np.nan
        
    # move Z mask
    mask_Z = myapply_dXdY_NN(Xr,Yr,mask_Z0,Xr,Yr,np.sum(Reca_Vec['dX']),np.sum(Reca_Vec['dY']))
    
    # calculate dH
    dH = myget_dH(DEMm,DEMr,mask,nNMAD)

    if tilt:
        # calculate dH
        dH = myget_dH(DEMm,DEMr,mask,nNMAD)

        # calculate dTilt
        dTilt = myFitTilt(dH,f_tilt, outDir = None)

        # apply dTilt to DEMm
        DEMm=myapply_dZ_2D(DEMm,dTilt)
        
        # calculate dH
        dH = myget_dH(DEMm,DEMr,mask,nNMAD)
    else:
        dTilt=np.array([[0,0],[0,0]])
        
    if Zcor:
        # calculate dZ from dH
        dZ=myget_dZ(dH,MeanMed,mask_Z)
            
        # apply dZ to DEMm
        DEMm=myapply_dZ(DEMm,dZ)
    
        # calculate dH
        dH = myget_dH(DEMm,DEMr,mask,nNMAD)    

    Reca_Vec['dZ'].append(dZ)

    # writing results
    print('\nTotal X shift :'+str(np.sum(Reca_Vec['dX'])))
    print('Total Y shift :'+str(np.sum(Reca_Vec['dY'])))
    print('Total Z shift :'+str(Reca_Vec['dZ'][-1]))    
    print('Final NMAD :'+str(Reca_Stat['NMAD_resi'][-1]))    
    print('Final std :'+str(Reca_Stat['STD_resi'][-1]))    
    print('Final mean :'+str(Reca_Stat['MEAN_resi'][-1]))    
    print('Final median :'+str(Reca_Stat['MEDIAN_resi'][-1]))    
    print('Tilt along track :'+str(np.round(dTilt[0,0]-dTilt[-1,0],3)))
    print('Tilt across track :'+str(np.round(dTilt[0,0]-dTilt[0,-1],3)))
    
    # writing report
    with open(outDir+'/log_reca_NnK.csv', 'w') as csvfile:
        writer  = csv.writer(csvfile, delimiter = ',')
        writer.writerow(['dX', 'dY', 'dZ', 'mean dH init', 'mean dH final', 'median dH init', 'median dH final', 'NMAD dH init', 'NMAD dH final', 'Tilt along', 'Tilt across'])
        writer.writerow([str(np.sum(Reca_Vec['dX'])), str(np.sum(Reca_Vec['dY'])), str(Reca_Vec['dZ'][-1]), str(Reca_Stat['MEAN_resi'][0]),str(Reca_Stat['MEAN_resi'][-1]), str(Reca_Stat['MEDIAN_resi'][0]),str(Reca_Stat['MEDIAN_resi'][-1]),str(Reca_Stat['NMAD_resi'][0]),str(Reca_Stat['NMAD_resi'][-1]),str(np.round(dTilt[0,0]-dTilt[-1,0],3)),str(np.round(dTilt[0,0]-dTilt[0,-1],3))])
    
    mymakeFig_dH(dH,Xr,Yr,outDir,'final')
    tmpName = os.path.basename(DEMmob)
    nameDEMmob = os.path.splitext(tmpName)[0]
    extDEMmob = os.path.splitext(tmpName)[1]
    if suffix is None:
        suffix = '_shifted_H_V'
    if suffix[0]!='_':
        suffix='_'+suffix
    tmpName = outDir+'/'+nameDEMmob+suffix+extDEMmob
    shutil.copy(DEMref,tmpName)
    ds = gdal.Open(tmpName, gdal.GA_Update)
    out_band = ds.GetRasterBand(1)
    out_band.SetNoDataValue(-9999)
    DEMm[ DEMm==np.float32(np.nan) ]=-9999
    DEMm[ DEMm<0 ]=-9999
    DEMm= np.flip( DEMm, 0)
    DEMm= myCleanDEM(DEMm, -9999)
    out_band.WriteArray(DEMm)
    ds = None
        
    print('\n END OF CO-REGISTRATRION \n')
                
    print('\n START OF JITTER CORECTION\n')
    ### Experimental
    # Corection of the jitter in the mobile DEM     
    DEMm= np.flip( DEMm, 0)
           
    dH[abs(dH)>1]=np.nan
    YmedianSTB=np.nanmedian(dH,axis=1)
    YmedianSTB[np.isnan(YmedianSTB)]=0
    YYSTB=(Yr[1]-Yr[0])*np.arange(0,len(Yr))
    ySTB = fft(YmedianSTB)
    xf = fftfreq(len(ySTB),abs(Yr[1]-Yr[0]))

    ## Corect from fit on stable terrain
    yt=np.copy(ySTB)
    yt[np.where(abs(1./xf)<2500)]=0
    Ytmp= ifft(yt)
    YSTBfit=np.real(np.sign(Ytmp))*np.abs(Ytmp)
    YSTBdetrend=YmedianSTB-YSTBfit
    
    Zundulation=np.repeat(np.expand_dims(YSTBfit,1),np.shape(dH)[1],axis=1)
    
    DEMm=myapply_dZ_2D(DEMm,Zundulation)
    
    # calculate dH
    dH = myget_dH(DEMm,DEMr,mask,nNMAD)

    # calculate dZ from dH
    dZ=myget_dZ(dH,MeanMed,mask_Z)
    
    # apply dZ to DEMm
    DEMm=myapply_dZ(DEMm,dZ)

    # calculate dH
    dH = myget_dH(DEMm,DEMr,mask,nNMAD)    
    mymakeFig_dH(dH,Xr,Yr,outDir,'jitter_det')


    plt.figure(33,figsize=(5,10))
    plt.plot(YmedianSTB,YYSTB,'.',alpha=1,label='STB terrain',linewidth=0.5,markersize=3)
    plt.plot(Ytmp,YYSTB,label='STB fit',color='c',linewidth=3)
    plt.plot(YSTBdetrend,YYSTB,label='STB corrected',color='r',linewidth=0.5)
    plt.vlines(0,np.min(YYSTB),np.max(YYSTB), color='k',linewidth=0.5)
    plt.ylim((np.min(YYSTB),np.max(YYSTB)))
    plt.xlim((-1.5,1.5))
    plt.legend(loc='best')
    plt.savefig(outDir+'/dH_jitter_det.svg')
    
    # Save moved DEM
    tmpName = outDir+'/'+nameDEMmob+suffix+'_jitter_det'+extDEMmob
    shutil.copy(DEMref,tmpName)
    ds = gdal.Open(tmpName, gdal.GA_Update)
    out_band = ds.GetRasterBand(1)
    out_band.SetNoDataValue(-9999)
    
    DEMm[ DEMm==np.float32(np.nan) ]=-9999
    DEMm[ DEMm<0 ]=-9999
    DEMm = np.flip( DEMm, 0)
    DEMm = myCleanDEM(DEMm, -9999)
    out_band.WriteArray(DEMm)
    ds = None
        
    print('\n END JITTER CORECTION\n')

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Co-register two DEMs. Mob(ile) moves to fit Ref(erence).")
    parser.add_argument("-DEMmob",dest = "DEMmob")
    parser.add_argument("-DEMref",dest = "DEMref")
    parser.add_argument("-SLPrst",dest = "SLPrst")
    parser.add_argument("-ASPrst",dest = "ASPrst")
    parser.add_argument("-MSKname",dest = "MSKname")
    parser.add_argument("-MSKZname",dest = "MSKZname")
    parser.add_argument("-thresholdNMAD",dest = "thresholdNMAD",type = float)
    parser.add_argument("-thresholdXYvec",dest = "thresholdXYvec",type = float)
    parser.add_argument("-Nmax",dest = "Nmax",type = int)
    parser.add_argument("-minSLP",dest = "minSLP",type = float)
    parser.add_argument("-maxSLP",dest = "maxSLP",type = float)
    parser.add_argument("-nNMAD",dest = "nNMAD", type = int)
    parser.add_argument("-outDir",dest = "outDir")
    parser.add_argument("-suffix",dest = "suffix")
    parser.add_argument("-OTB",dest = "OTB")
    parser.add_argument("-MeanMed",dest = "MeanMed", type = str)
    parser.add_argument("-tilt",dest = "tilt", type = int)
    parser.add_argument("-Zcor",dest = "Zcor", type = int)
    parser.add_argument("-maskSTB",dest = "maskSTB", type = int)    
    args = parser.parse_args()

    RecaNuthKaab_201028( args.DEMmob, args.DEMref, args.SLPrst, args.ASPrst, args.MSKname, args.MSKZname, args.thresholdNMAD, args.thresholdXYvec, args.Nmax, args.minSLP, args.maxSLP, args.nNMAD, args.outDir, args.suffix, args.OTB, args.MeanMed , args.tilt,args.Zcor,args.maskSTB)

