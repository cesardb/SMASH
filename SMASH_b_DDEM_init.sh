#!/bin/bash
# defining the parameters for SMASH_a_DEM.sh execution

# path to Orfeo Toolbox binary folder
OTB="/home/OTB/OTB-6.4.0-Linux64/bin"

# path to Ames Stereo Pipeline binary folder
ASP="/home/ASP/StereoPipeline-2.6.2-2019-06-17-x86_64-Linux/bin"

# date of the summer DEM (snow-off)
dateSummer=DATESUMMER

# list of the winter DEM date (snow-on) 
dateList=DATELIST #('yyyymmdd','yyyymmdd')

# folder containing the training vector files for land surface classification
trainFolder=""

# grid sampling distance (horizontal resolution) of the fine DEM
GSDfine=3

# folder where SMASH_a_DEM.sh are writen (DEM)
outputFolderDEM=""

# folder to write outputs (snow depth maps, classification)
outputFolderSNOWDEPTH0=""

# folder to write zip
zipFolder=""