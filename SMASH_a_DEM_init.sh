#!/bin/bash
# defining the parameters for SMASH_a_DEM.sh execution

# path to Orfeo Toolbox binary folder
OTB="/home/OTB/OTB-6.4.0-Linux64/bin"

# path to Ames Stereo Pipeline binary folder
ASP="/home/ASP/StereoPipeline-2.6.2-2019-06-17-x86_64-Linux/bin"

# path to ASP options file for coarse DEM production
st_file0="/home/script/stereo.default"
# path to ASP options file for fine DEM production
st_file="/home/script/stereo.default_SGM191015"

# grid sampling distance (horizontal resolution) of the coarse DEM
GSDcoarse=50
# grid sampling distance (horizontal resolution) of the fine DEM
GSDfine=3

# SRS code for the products
targetSRS="EPSG:32XXX"

# folder containing the stereo images and RPC files
dataFolderIMG=""

# folder to write outputs (DEM)
outputFolderDEM=""

# name of the image folder. should be : yyyymmdd+XX. XX being letters allowing distinctions between images acquired the same day (ex:N(orth),S(outh))
DATE=''